package com.epam.train.controller;

import com.epam.train.model.Student;
import com.epam.train.model.TooMuchStudents;

import java.util.List;

/**
 * Link model and view.
 */
public interface Controller {
    /**
     * @return list of students.
     * @throws TooMuchStudents exception.
     */
    List<Student> getStudentList() throws TooMuchStudents;

    /**
     * @return average mark of all students.
     */
    int getAverageMarkOfAllStudent();

    /**
     * Generate new student list.
     *
     * @throws TooMuchStudents exception.
     */
    void generateNewList() throws TooMuchStudents;

    /**
     * Refresh information in console.
     *
     * @throws TooMuchStudents exception.
     */
    void updateView() throws TooMuchStudents;
}
