package com.epam.train.controller;

import com.epam.train.model.Model;
import com.epam.train.model.Student;
import com.epam.train.model.TooMuchStudents;
import com.epam.train.view.View;

import java.util.List;

/**
 * Implements controller.
 */
public class ControllerImp implements Controller {
    /**
     * Model part of MVC.
     */
    private Model model;
    /**
     * View part of MVC.
     */
    private View view;

    /**
     * @param model data and business logic.
     * @param view  show information.
     * @throws TooMuchStudents exception.
     */
    public ControllerImp(final Model model, final View view)
            throws TooMuchStudents {
        this.model = model;
        this.view = view;
        view.setListOfStudents(getStudentList());
        view.setAverageMark(getAverageMarkOfAllStudent());
    }

    /**
     * @return list of students.
     * @throws TooMuchStudents exception.
     */
    @Override
    public final List<Student> getStudentList() throws TooMuchStudents {
        return model.getStudentList();
    }

    /**
     * @return average mark of all students.
     */
    @Override
    public final int getAverageMarkOfAllStudent() {
        return model.getAverageMarkOfAllStudents();
    }

    /**
     * Generate new list.
     */
    @Override
    public final void generateNewList() throws TooMuchStudents {
        model.generateStudents();
    }

    /**
     * Refresh information in console.
     */
    @Override
    public final void updateView() throws TooMuchStudents {
        String userChoice = view.printMenu();
        if (!userChoice.equalsIgnoreCase("Q")) {
            if (userChoice.equals("3")) {
                generateNewList();
                view.setListOfStudents(getStudentList());
                view.setAverageMark(getAverageMarkOfAllStudent());
            }
            view.show(userChoice);
            updateView();
        } else {
            System.out.println("Bye");
        }
    }

}
