package com.epam.train.view;

/**
 * Functional interface to print information in console.
 */
@FunctionalInterface
public interface Printable {
    /**
     * Print information in console.
     */
    void print();
}
