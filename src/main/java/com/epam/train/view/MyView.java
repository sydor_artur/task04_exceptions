package com.epam.train.view;

import com.epam.train.model.Student;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


/**
 * Display information to user.
 */
public class MyView implements View {
    /**
     * Contains all menu.
     */
    private Map<String, String> menu;
    /**
     * This variable links buttons and methods.
     */
    private Map<String, Printable> menuMethod;
    /**
     * This variable scan user input.
     */
    private Scanner scan = new Scanner(System.in);
    /**
     * List of all students.
     */
    private List<Student> listOfStudents;
    /**
     * This variable save average mark.
     */
    private int averageMark;

    /**
     * Constructor for class MyView.
     */
    public MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print list of students");
        menu.put("2", "2 - Show average mark between all students");
        menu.put("3", "3 - Generate new list of students");
        menu.put("Q", "Q - Quite");

        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::pressButton1);
        menuMethod.put("2", this::pressButton2);
        menuMethod.put("3", this::pressButton3);
    }

    /**
     * Log list of students in console.
     */
    private void pressButton1() {
        listOfStudents.forEach(student -> {
            System.out.println(student.toString());
        });
    }

    /**
     * Log average mark in console.
     */
    private void pressButton2() {
        System.out.println("Average mark of all students:" + averageMark);
    }

    /**
     * Generate new List of students.
     */
    private void pressButton3() {
        System.out.println("Generated new list!");
        pressButton1();
    }

    /**
     * Print menu.
     *
     * @return key for map menuMethod.
     */
    @Override
    public final String printMenu() {
        System.out.println("MENU");
        for (String str : menu.values()) {
            System.out.println(str);
        }
        String key;
        System.out.println("Make choice:");
        key = scan.nextLine().toUpperCase();
        return key;
    }

    /**
     * Show information in console.
     *
     * @param key for Menu.
     */
    public final void show(final String key) {
        try {
            menuMethod.get(key).print();
        } catch (Exception e) {
            System.out.println("Invalid input...");
        }
    }


    /**
     * @param listOfAllStudents is taken from controller.
     */
    public final void setListOfStudents(final List<Student> listOfAllStudents) {
        this.listOfStudents = listOfAllStudents;
    }

    /**
     * @param averageMarkOfStudents of all students.
     */
    public final void setAverageMark(final int averageMarkOfStudents) {
        this.averageMark = averageMarkOfStudents;
    }
}
