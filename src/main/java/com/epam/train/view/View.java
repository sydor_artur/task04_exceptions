package com.epam.train.view;

import com.epam.train.model.Student;

import java.util.List;

/**
 * Interface View contains main methods to
 * take data from controller and show.
 */
public interface View {
    /**
     * Prints menu.
     *
     * @return user choice.
     */
    String printMenu();

    /**
     * @param key for Menu.
     */
    void show(String key);

    /**
     * @param listOfStudents is taken from controller.
     */
    void setListOfStudents(List<Student> listOfStudents);

    /**
     * @param averageMark of all students.
     */
    void setAverageMark(int averageMark);
}
