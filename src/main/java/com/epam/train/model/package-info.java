/**
 * In this package are classes that realise
 * logic and generate data.
 */
package com.epam.train.model;
