package com.epam.train.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generate data.
 */
public class Domain implements Model {
    /**
     * Saves list of generated student.
     */
    private List<Student> students;
    /**
     * Saves average mark of all students.
     */
    private int averageMarkOfAllStudents;

    /**
     * Constructor.
     *
     * @throws TooMuchStudents exception.
     */
    public Domain() throws TooMuchStudents {
        generateStudents();
    }

    /**
     * @return List of all students.
     * @throws TooMuchStudents exception.
     */
    @Override
    public final List<Student> getStudentList() throws TooMuchStudents {
        return students;
    }

    /**
     * @return average mark of all students.
     */
    @Override
    public final int getAverageMarkOfAllStudents() {
        return averageMarkOfAllStudents;
    }

    /**
     * Generate random student.
     *
     * @throws InvalidListOfStudent if size of list equals zero.
     * @throws TooMuchStudents      exception.
     */
    public final void generateStudents()
            throws InvalidListOfStudent, TooMuchStudents {
        Student.setNumberOfStudents(0);
        Random rand = new Random();
        int numberOfStudents = rand.nextInt(20 + 1);
        students = new ArrayList<>();
        for (int i = 0; i < numberOfStudents; i++) {
            try (Student student = new Student(rand.nextInt(12 + 1))) {
                students.add(student);
            }
        }
        if (students.size() == 0) {
            throw new InvalidListOfStudent("List of a zero size");
        }
        calculateAverageMark();
    }

    /**
     * @return average mark of all students.
     */
    private int calculateAverageMark() {
        if (Student.getNumberOfStudents() != 0) {
            students.forEach(student -> {
                averageMarkOfAllStudents += student.getAverageMark();
            });
            averageMarkOfAllStudents /= Student.getNumberOfStudents();
            return averageMarkOfAllStudents;
        } else {
            return 0;
        }
    }
}
