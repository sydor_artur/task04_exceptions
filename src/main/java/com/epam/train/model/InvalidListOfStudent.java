package com.epam.train.model;

/**
 * This exception is thrown when list size equals zero.
 */
public class InvalidListOfStudent extends RuntimeException {
    /**
     * @param message that describes exception.
     */
    public InvalidListOfStudent(final String message) {
        super(message);
    }

    /**
     * @param message that describes exception.
     * @param cause   of exception.
     */
    public InvalidListOfStudent(final String message, final Throwable cause) {
        super(message, cause);
    }
}
