package com.epam.train.model;

import java.util.List;

/**
 * Interface Model take data from domain.
 */
public interface Model {
    /**
     * @return List of all students.
     * @throws TooMuchStudents exception.
     */
    List<Student> getStudentList() throws TooMuchStudents;

    /**
     * @return average mark of all students.
     */
    int getAverageMarkOfAllStudents();

    /**
     * Generate list of students.
     *
     * @throws TooMuchStudents exception.
     */
    void generateStudents() throws TooMuchStudents;
}
