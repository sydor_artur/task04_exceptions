package com.epam.train.model;

/**
 * Exception that controll number of students.
 */
public class TooMuchStudents extends Exception {
    /**
     * @param message describe exception.
     */
    public TooMuchStudents(final String message) {
        super(message);
    }

    /**
     * @param message describe exception.
     * @param cause   of exception.
     */
    public TooMuchStudents(final String message, final Throwable cause) {
        super(message, cause);
    }
}
