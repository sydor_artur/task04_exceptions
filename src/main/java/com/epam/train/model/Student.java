package com.epam.train.model;

/**
 * Describe student.
 */
public class Student implements AutoCloseable {
    /**
     * Name of student.
     */
    private String name;
    /**
     * Variable that save average mark of certain student.
     */
    private int averageMark;
    /**
     * Variable that save number of students.
     */
    private static int numberOfStudents;

    /**
     * @param averageMarkOfAllStudents of certain student.
     */
    public Student(final int averageMarkOfAllStudents) {
        name = "Student#" + ++numberOfStudents;
        this.averageMark = averageMarkOfAllStudents;

    }

    /**
     * @return name of student.
     */
    public final String getName() {
        return name;
    }

    /**
     * @return average mark of all students.
     */
    public final int getAverageMark() {
        return averageMark;
    }

    /**
     * @return string , that describe student.
     */
    @Override
    public final String toString() {
        return name + ", average mark:" + averageMark;
    }

    /**
     * @return number of students.
     */
    public static final int getNumberOfStudents() {
        return numberOfStudents;
    }

    /**
     *
     * @param numberOfAllStudents in list.
     */
    public static void setNumberOfStudents(final int numberOfAllStudents) {
        Student.numberOfStudents = numberOfAllStudents;
    }

    /**
     * @throws TooMuchStudents exception about illegal number of students.
     */
    @Override
    public final void close() throws TooMuchStudents {
        if (numberOfStudents > 10) {
            throw new TooMuchStudents("You created too much students:"
                    + numberOfStudents);
        }
    }
}
