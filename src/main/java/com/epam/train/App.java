package com.epam.train;

import com.epam.train.controller.Controller;
import com.epam.train.controller.ControllerImp;
import com.epam.train.model.Domain;
import com.epam.train.model.Model;
import com.epam.train.model.TooMuchStudents;
import com.epam.train.view.MyView;
import com.epam.train.view.View;

/**
 * Utility class.
 */
public final class App {
    /**
     * Constructor of utility class.
     */
    private App() {
    }

    /**
     * Start point of application.
     *
     * @param args form user.
     */
    public static void main(final String[] args) {
        Model model = null;
        try {
            model = new Domain();
        } catch (TooMuchStudents e) {
            System.err.println(e);
        }
        View myView = new MyView();
        Controller controller = null;
        try {
            controller = new ControllerImp(model, myView);
            controller.updateView();
        } catch (TooMuchStudents e) {
            System.err.println(e);
        }
    }
}
